import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/firestore';
import { useState, useEffect } from 'react';

const firebaseConfig = {
  apiKey: "AIzaSyAfQ0lmzfs_6k8XcF5LCA7B7yMsLORl2l4",
  authDomain: "lunch-and-learn-app.firebaseapp.com",
  projectId: "lunch-and-learn-app",
  storageBucket: "lunch-and-learn-app.appspot.com",
  messagingSenderId: "345936423239",
  appId: "1:345936423239:web:55c4c6c394524cc29b4462"
};

function App() {
  const [app, setApp] = useState();
  const [user, setUser] = useState();
  const [db, setDatabase] = useState();
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  const signIn = () => {
    const provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithPopup(provider).then(setUser)
  }

  const sendMessage = () => {
    db.collection('chats')
    .add({
      message,
      user: {
        uid: user.uid,
        name: user.displayName,
      },
      createdAt: Date.now(),
    }).then(() => setMessage(''))
  }

  useEffect(() => {
    if (!app) {
      if (firebase.apps.length) setApp(firebase.apps[0]);
      else setApp(firebase.initializeApp(firebaseConfig))
      setDatabase(firebase.firestore())
    };

    return setApp;
  }, []);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(setUser);
    return setUser;
  }, [])

  useEffect(() => {
    if (db) {
      return db.collection("chats").orderBy("createdAt").onSnapshot((querySnapshot) => {
        var docs = [];
        querySnapshot.forEach((doc) => {
          docs.push(doc.data());
        });
        setMessages(docs);
      })
    }
  }, [db])

  console.log(messages);

  if (!app) return null;

  if (!user) return <button onClick={signIn}>Sign In</button>

  return (
    <div>
      <div>
        {messages.map((message) => {
          return (<p key={message.createdAt}>
            <span>{message.user.name}</span>
            <br/>
            {message.message}
          </p>)
        })}
      </div>
      <div>
        <textarea value={message} onChange={({target: {value}}) => setMessage(value)}></textarea>
        <button onClick={sendMessage}>Send</button>
      </div>
    </div>
  );
}

export default App;
